fn main() {
    let input_str = std::fs::read_to_string("data/_.txt").unwrap();
    let input = parse(&input_str);

    println!("1. {}", part_one(&input));
    // println!("2. {}", part_two(&input));
}

fn parse(input: &str) -> Vec<String> {
    input.lines().map(|l| l.to_string()).collect()
}

fn part_one(input: &[String]) -> usize {
    0
}

// fn part_two(input: &[String]) -> usize {
//     0
// }
