#[derive(Debug, Clone, Copy, Eq, PartialEq)]
enum Spot {
    Floor,
    EmptySeat,
    OccupiedSeat,
}

fn main() {
    let input_str = std::fs::read_to_string("data/11.txt").unwrap();
    let world = parse(&input_str);

    println!("1. {}", part_one(&world));
    println!("2. {}", part_two(&world));
}

fn parse(input: &str) -> Vec<Vec<Spot>> {
    input
        .lines()
        .map(|row| {
            row.chars()
                .map(|col| match col {
                    '.' => Spot::Floor,
                    'L' => Spot::EmptySeat,
                    _ => panic!("Unexpected input"),
                })
                .collect()
        })
        .collect()
}

fn part_one(world: &[Vec<Spot>]) -> usize {
    let mut old_world = world.clone().to_owned();
    loop {
        let new_world = tick_one(&old_world);
        if old_world == new_world {
            break;
        }
        old_world = new_world.to_owned();
    }

    old_world
        .iter()
        .map(|row| {
            row.iter()
                .filter(|&spot| spot == &Spot::OccupiedSeat)
                .count()
        })
        .sum()
}

fn tick_one(world: &[Vec<Spot>]) -> Vec<Vec<Spot>> {
    let mut new_world = world.clone().to_owned();

    let max_col_idx = world[0].len() - 1;
    let max_row_idx = world.len() - 1;

    for (row_idx, row) in new_world.iter_mut().enumerate() {
        for (col_idx, spot) in row.iter_mut().enumerate() {
            if *spot == Spot::Floor {
                continue;
            }

            let mut adjacent_occupied_seats = 0;
            let row_range = Ord::max(1, row_idx) - 1..=Ord::min(max_row_idx, row_idx + 1);
            for check_row_idx in row_range {
                let col_range = Ord::max(1, col_idx) - 1..=Ord::min(max_col_idx, col_idx + 1);
                for check_col_idx in col_range {
                    if check_row_idx == row_idx && check_col_idx == col_idx {
                        continue;
                    }
                    if world[check_row_idx][check_col_idx] == Spot::OccupiedSeat {
                        adjacent_occupied_seats += 1;
                    }
                }
            }

            if adjacent_occupied_seats >= 4 {
                *spot = Spot::EmptySeat;
            } else if adjacent_occupied_seats == 0 {
                *spot = Spot::OccupiedSeat;
            }
        }
    }

    new_world.to_vec()
}

fn part_two(world: &[Vec<Spot>]) -> usize {
    let mut old_world = world.clone().to_owned();
    loop {
        let new_world = tick_two(&old_world);
        if old_world == new_world {
            break;
        }
        old_world = new_world.to_owned();
    }

    old_world
        .iter()
        .map(|row| {
            row.iter()
                .filter(|&spot| spot == &Spot::OccupiedSeat)
                .count()
        })
        .sum()
}

fn tick_two(world: &[Vec<Spot>]) -> Vec<Vec<Spot>> {
    let mut new_world = world.clone().to_owned();

    let max_col_idx = world[0].len() - 1;
    let max_row_idx = world.len() - 1;

    for (row_idx, row) in new_world.iter_mut().enumerate() {
        for (col_idx, spot) in row.iter_mut().enumerate() {
            if *spot == Spot::Floor {
                continue;
            }

            let first_north = (0..row_idx)
                .rev()
                .map(|row| world[row][col_idx])
                .filter(|&spot| spot != Spot::Floor)
                .next();
            let first_northeast = (0..row_idx)
                .rev()
                .zip((0..col_idx).rev())
                .map(|(row, col)| world[row][col])
                .filter(|&spot| spot != Spot::Floor)
                .next();
            let first_east = (0..col_idx)
                .rev()
                .map(|col| world[row_idx][col])
                .filter(|&spot| spot != Spot::Floor)
                .next();
            let first_southeast = (row_idx + 1..=max_row_idx)
                .zip((0..col_idx).rev())
                .map(|(row, col)| world[row][col])
                .filter(|&spot| spot != Spot::Floor)
                .next();
            let first_south = (row_idx + 1..=max_row_idx)
                .map(|row| world[row][col_idx])
                .filter(|&spot| spot != Spot::Floor)
                .next();
            let first_southwest = (row_idx + 1..=max_row_idx)
                .zip(col_idx + 1..=max_col_idx)
                .map(|(row, col)| world[row][col])
                .filter(|&spot| spot != Spot::Floor)
                .next();
            let first_west = (col_idx + 1..=max_col_idx)
                .map(|col| world[row_idx][col])
                .filter(|&spot| spot != Spot::Floor)
                .next();
            let first_northwest = (0..row_idx)
                .rev()
                .zip(col_idx + 1..=max_col_idx)
                .map(|(row, col)| world[row][col])
                .filter(|&spot| spot != Spot::Floor)
                .next();

            let visible_occupied_seats: usize = vec![
                first_north,
                first_northeast,
                first_east,
                first_southeast,
                first_south,
                first_southwest,
                first_west,
                first_northwest,
            ]
            .iter()
            .map(|&spot| {
                if spot == Some(Spot::OccupiedSeat) {
                    1
                } else {
                    0
                }
            })
            .sum();

            if visible_occupied_seats >= 5 {
                *spot = Spot::EmptySeat;
            } else if visible_occupied_seats == 0 {
                *spot = Spot::OccupiedSeat;
            }
        }
    }

    new_world.to_vec()
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn tick_one_first_iter() {
        let input = "L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL
"
        .to_string();
        let expected = "#.##.##.##
#######.##
#.#.#..#..
####.##.##
#.##.##.##
#.#####.##
..#.#.....
##########
#.######.#
#.#####.##
"
        .to_string();
        assert_eq!(fmt_world(&tick_one(&parse(&input))), expected);
    }

    #[test]
    fn tick_one_second_iter() {
        let input = "L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL
"
        .to_string();
        let expected = "#.LL.L#.##
#LLLLLL.L#
L.L.L..L..
#LLL.LL.L#
#.LL.LL.LL
#.LLLL#.##
..L.L.....
#LLLLLLLL#
#.LLLLLL.L
#.#LLLL.##
"
        .to_string();
        assert_eq!(fmt_world(&tick_one(&tick_one(&parse(&input)))), expected);
    }
}
