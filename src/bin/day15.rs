use std::collections::HashMap;

fn main() {
    let input = vec![15, 12, 0, 14, 3, 1];
    println!("1. {}", part_one(&input));
}

fn part_one(input: &[usize]) -> usize {
    let mut last_seen: HashMap<usize, usize> = HashMap::new();

    input
        .iter()
        .enumerate()
        .for_each(|(turn, &starting_number)| {
            last_seen.insert(starting_number, turn + 1);
        });

    let mut last_number = input[input.len() - 1];
    let mut next_number;
    let mut last_number_was_brand_new = true;
    for current_turn in (input.len() + 1)..=30_000_000 {
        if last_number_was_brand_new {
            next_number = 0;
        } else {
            next_number = current_turn - 1 - last_seen.get(&last_number).unwrap();
        }

        last_number_was_brand_new = !last_seen.contains_key(&next_number);
        last_seen.insert(last_number, current_turn - 1);
        last_number = next_number;
    }

    last_number
}
