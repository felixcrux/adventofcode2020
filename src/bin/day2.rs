use std::fs;
use std::io::{self, BufRead};

#[derive(Debug, Clone)]
struct PasswordPolicy {
    lower_char: usize,
    upper_char: usize,
    required_char: u8,
}

fn main() {
    let policy_and_password_re = regex::Regex::new(
        r"(?P<lower_char>\d+)-(?P<upper_char>\d+) (?P<required_char>.): (?P<password>.+)",
    )
    .unwrap();

    let file = fs::File::open("data/2.txt").unwrap();
    let input = io::BufReader::new(file)
        .lines()
        .map(Result::unwrap)
        .map(|input_line| {
            let parsed_line = policy_and_password_re.captures(&input_line).unwrap();

            return (
                PasswordPolicy {
                    lower_char: parsed_line
                        .name("lower_char")
                        .unwrap()
                        .as_str()
                        .parse::<usize>()
                        .unwrap(),
                    upper_char: parsed_line
                        .name("upper_char")
                        .unwrap()
                        .as_str()
                        .parse::<usize>()
                        .unwrap(),
                    required_char: *parsed_line
                        .name("required_char")
                        .unwrap()
                        .as_str()
                        .as_bytes()
                        .first()
                        .unwrap(),
                },
                parsed_line
                    .name("password")
                    .unwrap()
                    .as_str()
                    .bytes()
                    .collect::<Vec<u8>>(),
            );
        })
        .collect::<Vec<(PasswordPolicy, Vec<u8>)>>();

    println!("1. {}", part_one(input.iter()));
    println!("2. {}", part_two(input.iter()));
}

fn part_one<'a>(input: impl Iterator<Item = &'a (PasswordPolicy, Vec<u8>)>) -> usize {
    input
        .filter(|(policy, password)| {
            let required_chars = password
                .iter()
                .filter(|c| *c == &policy.required_char)
                .count();
            required_chars >= policy.lower_char && required_chars <= policy.upper_char
        })
        .count()
}

fn part_two<'a>(input: impl Iterator<Item = &'a (PasswordPolicy, Vec<u8>)>) -> usize {
    input
        .filter(|(policy, password)| {
            (password[policy.lower_char - 1] != password[policy.upper_char - 1])
                && ((password[policy.lower_char - 1] == policy.required_char)
                    || (password[policy.upper_char - 1] == policy.required_char))
        })
        .count()
}
