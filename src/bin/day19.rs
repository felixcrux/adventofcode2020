use std::collections::HashMap;

type Rules = HashMap<usize, Vec<RuleToken>>;

#[derive(Debug, Eq, PartialEq)]
enum RuleToken {
    Literal(String),
    RuleRef(usize),
    Or,
}

fn main() {
    let input_str = include_str!("../../data/19.txt");
    let (rules, messages) = parse(&input_str);

    println!("1. {}", part_one(&rules, &messages));
    // println!("2. {}", part_two(&input));
}

fn parse(input: &str) -> (Rules, Vec<String>) {
    let mut lines = input.lines();
    let rules = lines
        .by_ref()
        .take_while(|l| *l != "")
        .map(|l| {
            let line = l.to_string();
            let mut id_and_rule = line.split(": ");
            let id = id_and_rule.next().unwrap().parse::<usize>().unwrap();
            let raw_rule = id_and_rule.next().unwrap();
            let rule_parts = raw_rule
                .split_ascii_whitespace()
                .map(|token| {
                    if token == "|" {
                        return RuleToken::Or;
                    }
                    if token.starts_with('"') {
                        return RuleToken::Literal(token.trim_matches('"').to_owned());
                    }
                    return RuleToken::RuleRef(token.parse::<usize>().unwrap());
                })
                .collect();
            (id, rule_parts)
        })
        .collect();
    let messages = lines.map(std::string::ToString::to_string).collect();

    (rules, messages)
}

fn simpify_rule(rules: &Rules, rule_id: usize) -> String {
    rules[&rule_id]
        .iter()
        .map(|token| match token {
            RuleToken::Or => "|".to_string(),
            RuleToken::RuleRef(id) => "(".to_string() + &simpify_rule(rules, *id) + ")",
            RuleToken::Literal(lit) => lit.to_string(),
        })
        .collect()
}

fn part_one(rules: &Rules, messages: &[String]) -> usize {
    let full_rule = "^".to_string() + &simpify_rule(rules, 0) + "$";
    let rule_re = regex::Regex::new(&full_rule).unwrap();

    messages.iter().filter(|m| rule_re.is_match(&m)).count()
}

// fn part_two(input: &[String]) -> usize {
//     0
// }
