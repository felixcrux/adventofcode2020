use std::collections::HashSet;
use std::fs;
use std::io::Read;

#[derive(Debug)]
struct State {
    acc: isize,
    ip: usize,
}

#[derive(Debug, Eq, PartialEq, Copy, Clone)]
enum Instruction {
    NOp(isize),
    Jmp(isize),
    Acc(isize),
}

fn main() {
    let mut file = fs::File::open("data/8.txt").unwrap();
    let mut input_str = String::new();
    file.read_to_string(&mut input_str).unwrap();

    let instructions = parse(&input_str);

    println!("1. {}", part_one(&instructions));
    println!("2. {}", part_two(&instructions));
}

fn parse(input: &str) -> Vec<Instruction> {
    input
        .lines()
        .map(|line| {
            let mut split_line = line.split(' ');
            let opcode = split_line.next().unwrap();
            let arg = split_line.next().unwrap().parse().unwrap();
            match opcode {
                "nop" => Instruction::NOp(arg),
                "jmp" => Instruction::Jmp(arg),
                "acc" => Instruction::Acc(arg),
                _ => panic!("Unknown instruction"),
            }
        })
        .collect()
}

fn tick(state: &mut State, program: &[Instruction]) {
    match program[state.ip] {
        Instruction::NOp(_) => {
            state.ip += 1;
        }
        Instruction::Jmp(dst) => {
            state.ip = (state.ip as isize + dst) as usize;
        }
        Instruction::Acc(val) => {
            state.acc += val;
            state.ip += 1;
        }
    }
}

fn part_one(program: &[Instruction]) -> isize {
    let mut seen = HashSet::<usize>::new();
    let mut state = State { acc: 0, ip: 0 };
    loop {
        if seen.contains(&state.ip) {
            return state.acc;
        }
        seen.insert(state.ip);
        tick(&mut state, program);
    }
}

fn part_two(program: &[Instruction]) -> isize {
    let mut potential_solutions = Vec::new();
    part_two_helper(program.len(), program, &mut potential_solutions);

    for (idx, inst) in &potential_solutions {
        let mut modified_program = program.clone().to_owned();
        modified_program[*idx] = match inst {
            Instruction::NOp(arg) => Instruction::Jmp(*arg),
            Instruction::Jmp(arg) => Instruction::NOp(*arg),
            _ => panic!("Unexpected instruction"),
        };

        let mut seen = HashSet::<usize>::new();
        let mut state = State { acc: 0, ip: 0 };
        loop {
            if state.ip >= program.len() {
                return state.acc;
            }
            if seen.contains(&state.ip) {
                break;
            }
            seen.insert(state.ip);
            tick(&mut state, &modified_program);
        }
    }

    panic!("No solution");
}

fn part_two_helper(
    target: usize,
    program: &[Instruction],
    potential_solutions: &mut Vec<(usize, Instruction)>,
) {
    let hypotheticals = generate_instructions_that_would_lead_to(target, program.len() - 1);

    potential_solutions.extend(check_hypotheticals_for_potential_solutions(
        &hypotheticals,
        program,
    ));

    let pruned_hypotheticals = prune_hypotheticals(&hypotheticals, program);
    for (ip, _) in &pruned_hypotheticals {
        part_two_helper(*ip, program, potential_solutions);
    }
}

fn generate_instructions_that_would_lead_to(
    target_ip: usize,
    max_ip: usize,
) -> Vec<(usize, Instruction)> {
    let mut hypotheticals = vec![
        (target_ip - 1, Instruction::NOp(0)),
        (target_ip - 1, Instruction::Acc(0)),
    ];

    hypotheticals.extend(
        (0..target_ip)
            .chain((target_ip + 1)..=max_ip)
            .map(|hypothetical_ip| {
                (
                    hypothetical_ip,
                    Instruction::Jmp(target_ip as isize - hypothetical_ip as isize),
                )
            }),
    );

    hypotheticals
}

fn check_hypotheticals_for_potential_solutions(
    hypotheticals: &[(usize, Instruction)],
    program: &[Instruction],
) -> Vec<(usize, Instruction)> {
    let mut potentials = Vec::new();
    for &(ip, hypothetical) in hypotheticals {
        let actual = program[ip];
        match actual {
            Instruction::NOp(arg) => {
                if hypothetical == Instruction::Jmp(arg) {
                    potentials.push((ip, actual));
                }
            }
            Instruction::Jmp(_) => {
                if hypothetical == Instruction::NOp(0) {
                    potentials.push((ip, actual));
                }
            }
            Instruction::Acc(_) => {}
        }
    }
    potentials
}

fn prune_hypotheticals(
    hypotheticals: &[(usize, Instruction)],
    program: &[Instruction],
) -> Vec<(usize, Instruction)> {
    hypotheticals
        .iter()
        .filter(|(ip, hypothetical)| match program[*ip] {
            Instruction::Jmp(dst) => Instruction::Jmp(dst) == *hypothetical,
            Instruction::NOp(_) => Instruction::NOp(0) == *hypothetical,
            Instruction::Acc(_) => Instruction::Acc(0) == *hypothetical,
        })
        .cloned()
        .collect()
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_generate_instructions_leading_to_after_last_instruction() {
        let target_ip = 1;
        let max_ip = 0;
        let expected = vec![
            (0, Instruction::NOp(0)),
            (0, Instruction::Acc(0)),
            (0, Instruction::Jmp(1)),
        ];

        assert_eq!(
            generate_instructions_that_would_lead_to(target_ip, max_ip),
            expected
        );
    }

    #[test]
    fn test_generate_instructions_leading_to_after_last_instruction_in_longer_program() {
        let target_ip = 5;
        let max_ip = 4;
        let expected = vec![
            (4, Instruction::NOp(0)),
            (4, Instruction::Acc(0)),
            (0, Instruction::Jmp(5)),
            (1, Instruction::Jmp(4)),
            (2, Instruction::Jmp(3)),
            (3, Instruction::Jmp(2)),
            (4, Instruction::Jmp(1)),
        ];

        assert_eq!(
            generate_instructions_that_would_lead_to(target_ip, max_ip),
            expected
        );
    }

    #[test]
    fn test_generate_instructions_leading_to_very_last_instruction() {
        let target_ip = 1;
        let max_ip = 1;
        let expected = vec![
            (0, Instruction::NOp(0)),
            (0, Instruction::Acc(0)),
            (0, Instruction::Jmp(1)),
        ];

        assert_eq!(
            generate_instructions_that_would_lead_to(target_ip, max_ip),
            expected
        );
    }

    #[test]
    fn test_generate_instructions_leading_to_very_last_instruction_in_longer_program() {
        let target_ip = 4;
        let max_ip = 4;
        let expected = vec![
            (3, Instruction::NOp(0)),
            (3, Instruction::Acc(0)),
            (0, Instruction::Jmp(4)),
            (1, Instruction::Jmp(3)),
            (2, Instruction::Jmp(2)),
            (3, Instruction::Jmp(1)),
        ];

        assert_eq!(
            generate_instructions_that_would_lead_to(target_ip, max_ip),
            expected
        );
    }

    #[test]
    fn test_generate_instructions_leading_to_middle_of_program() {
        let target_ip = 3;
        let max_ip = 5;
        let expected = vec![
            (2, Instruction::NOp(0)),
            (2, Instruction::Acc(0)),
            (0, Instruction::Jmp(3)),
            (1, Instruction::Jmp(2)),
            (2, Instruction::Jmp(1)),
            (4, Instruction::Jmp(-1)),
            (5, Instruction::Jmp(-2)),
        ];

        assert_eq!(
            generate_instructions_that_would_lead_to(target_ip, max_ip),
            expected
        );
    }
}
