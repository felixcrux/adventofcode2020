use std::collections::HashSet;

type Ticket = Vec<usize>;
type FieldRanges = (String, Vec<std::ops::RangeInclusive<usize>>);

fn main() {
    let input_str = include_str!("../../data/16.txt");
    let (fields_and_ranges, my_ticket, nearby_tickets) = parse(&input_str);

    println!("1. {}", part_one(&fields_and_ranges, &nearby_tickets));
    println!(
        "2. {}",
        part_two(&fields_and_ranges, &my_ticket, &nearby_tickets)
    );
}

fn parse(input: &str) -> (Vec<FieldRanges>, Ticket, Vec<Ticket>) {
    let mut input_lines = input.lines();

    let mut fields_and_ranges = vec![];
    while let Some(line) = input_lines.next() {
        if line == "" {
            break;
        }
        let mut field_and_ranges_as_strs = line.split(": ");
        let name = field_and_ranges_as_strs.next().unwrap().to_owned();
        let ranges = field_and_ranges_as_strs
            .next()
            .unwrap()
            .split(" or ")
            .map(|range_as_str| {
                let mut bounds = range_as_str.split('-');
                let lower = bounds.next().unwrap().parse::<usize>().unwrap();
                let upper = bounds.next().unwrap().parse::<usize>().unwrap();
                lower..=upper
            })
            .collect();
        fields_and_ranges.push((name, ranges));
    }

    assert_eq!(input_lines.next(), Some("your ticket:"));
    let my_ticket = input_lines
        .next()
        .unwrap()
        .split(',')
        .map(|field| field.parse::<usize>().unwrap())
        .collect();

    assert_eq!(input_lines.next(), Some(""));
    assert_eq!(input_lines.next(), Some("nearby tickets:"));
    let nearby_tickets = input_lines
        .map(|line| {
            line.split(',')
                .map(|field| field.parse::<usize>().unwrap())
                .collect()
        })
        .collect();

    (fields_and_ranges, my_ticket, nearby_tickets)
}

fn part_one(fields_and_ranges: &[FieldRanges], nearby_tickets: &[Ticket]) -> usize {
    let mut error_rate = 0;
    for ticket in nearby_tickets {
        for ticket_field in ticket {
            if !fields_and_ranges
                .iter()
                .any(|(_, ranges)| ranges.iter().any(|range| range.contains(ticket_field)))
            {
                error_rate += ticket_field;
            }
        }
    }

    error_rate
}

fn part_two(
    fields_and_ranges: &[FieldRanges],
    my_ticket: &Ticket,
    nearby_tickets: &[Ticket],
) -> usize {
    let valid_tickets = nearby_tickets
        .iter()
        .filter(|ticket| {
            ticket.iter().all(|ticket_field| {
                fields_and_ranges
                    .iter()
                    .any(|(_, ranges)| ranges.iter().any(|range| range.contains(ticket_field)))
            })
        })
        .cloned()
        .collect::<Vec<Ticket>>();

    let mut hypotheses: Vec<HashSet<usize>> = (0..my_ticket.len())
        .map(|_| {
            let mut h = HashSet::new();
            (0..fields_and_ranges.len()).for_each(|i| {
                h.insert(i);
            });
            h
        })
        .collect();

    for ticket in valid_tickets {
        for (ticket_field_idx, field) in ticket.iter().enumerate() {
            for (hypothetical_field_idx, (_, ranges)) in fields_and_ranges.iter().enumerate() {
                if !ranges.iter().any(|range| range.contains(field)) {
                    hypotheses[ticket_field_idx].remove(&hypothetical_field_idx);
                }
            }
        }
    }

    loop {
        let solved = hypotheses
            .iter()
            .enumerate()
            .filter(|(_, h)| h.len() == 1)
            .map(|(idx, h)| (idx, h.iter().next().unwrap().to_owned()))
            .collect::<Vec<(usize, usize)>>();
        if solved.len() == my_ticket.len() {
            break;
        }
        for (field_idx, solution_idx) in solved {
            hypotheses.iter_mut().enumerate().for_each(|(h_idx, h)| {
                if h_idx != field_idx {
                    h.remove(&solution_idx);
                }
            });
        }
    }

    hypotheses
        .iter()
        .enumerate()
        .map(|(i, h)| (i, h.iter().next().unwrap().to_owned()))
        .filter_map(|(i, s)| {
            if fields_and_ranges[s].0.starts_with("departure") {
                Some(my_ticket[i])
            } else {
                None
            }
        })
        .product()
}
