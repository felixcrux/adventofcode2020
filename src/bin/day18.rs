#[derive(Eq, PartialEq, Clone, Copy)]
enum Token {
    Literal(usize),
    Operator(char),
    OpenParen,
    CloseParen,
}

impl Token {
    fn precedence(&self) -> usize {
        match self {
            Token::Operator('+') => 2,
            Token::Operator('*') => 1,
            _ => 0,
        }
    }
}

impl std::fmt::Debug for Token {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Token::Literal(val) => write!(f, "L[{}] ", val),
            Token::Operator(op) => write!(f, "O[{}] ", op),
            Token::OpenParen => write!(f, "( "),
            Token::CloseParen => write!(f, ") "),
        }
    }
}

struct Tokenized(Vec<Token>);

impl std::fmt::Debug for Tokenized {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.iter().map(|t| Token::fmt(t, f)).collect()
    }
}

#[derive(Debug)]
enum ParseTree {
    Constant(usize),
    Expression {
        operator: char,
        lhs: Box<ParseTree>,
        rhs: Box<ParseTree>,
    },
}

fn main() {
    let input_str = include_str!("../../data/18.txt");
    let input = parse_lines(&input_str);

    println!("1. {}", compute(&input));
}

fn tokenize(input: &[char]) -> Tokenized {
    let mut tokens = vec![];

    let mut literal_buffer = String::new();
    for c in input {
        match c {
            ' ' => {
                if literal_buffer != "" {
                    tokens.push(Token::Literal(literal_buffer.parse::<usize>().unwrap()));
                    literal_buffer.clear();
                }
            }
            '+' | '*' => tokens.push(Token::Operator(*c)),
            '(' => tokens.push(Token::OpenParen),
            ')' => {
                if literal_buffer != "" {
                    tokens.push(Token::Literal(literal_buffer.parse::<usize>().unwrap()));
                    literal_buffer.clear();
                }
                tokens.push(Token::CloseParen)
            }
            _ => literal_buffer.push(*c),
        };
    }
    if literal_buffer != "" {
        tokens.push(Token::Literal(literal_buffer.parse::<usize>().unwrap()));
        literal_buffer.clear();
    }

    Tokenized(tokens)
}

fn parse(input: &Tokenized) -> ParseTree {
    parse_with_precedence(&mut input.0.iter().peekable(), 0)
}

fn parse_with_precedence(
    input: &mut std::iter::Peekable<std::slice::Iter<Token>>,
    prev_precedence: usize,
) -> ParseTree {
    let mut token = input.next();
    let mut lhs = match token.unwrap() {
        Token::Literal(val) => ParseTree::Constant(*val),
        Token::OpenParen => {
            let subexpr = parse_with_precedence(input, 0);
            input.next();
            subexpr
        }
        _ => panic!(format!("{:?}", token.unwrap())),
    };
    let mut next_token = input.peek();
    while next_token.is_some()
        && **next_token.unwrap() != Token::CloseParen
        && prev_precedence <= next_token.unwrap().precedence()
    {
        token = input.next();
        lhs = match token.unwrap() {
            Token::OpenParen => {
                let subexpr = parse_with_precedence(input, 0);
                input.next();
                subexpr
            }
            Token::Operator(op) => ParseTree::Expression {
                operator: *op,
                lhs: Box::new(lhs),
                rhs: Box::new(parse_with_precedence(input, token.unwrap().precedence())),
            },
            _ => panic!(format!("{:?}", token.unwrap())),
        };
        next_token = input.peek();
    }

    lhs
}

fn parse_lines(input: &str) -> Vec<ParseTree> {
    input
        .lines()
        .map(|l| parse(&tokenize(&l.chars().collect::<Vec<char>>())))
        .collect()
}

fn eval(input: &ParseTree) -> usize {
    match input {
        ParseTree::Constant(val) => *val,
        ParseTree::Expression {
            operator: '*',
            lhs,
            rhs,
        } => eval(lhs) * eval(rhs),
        ParseTree::Expression {
            operator: '+',
            lhs,
            rhs,
        } => eval(lhs) + eval(rhs),
        _ => panic!("unhandled"),
    }
}

fn compute(input: &[ParseTree]) -> usize {
    input.iter().map(|l| eval(&l)).sum()
}
