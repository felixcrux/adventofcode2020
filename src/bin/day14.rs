use std::collections::HashMap;

type MaskBit = bool;

#[derive(Debug)]
enum Instruction {
    Mask(Vec<Option<MaskBit>>),
    Mem { destination: usize, value: usize },
}

fn main() {
    let input_str = std::fs::read_to_string("data/14.txt").unwrap();
    let input = parse(&input_str);

    println!("1. {}", part_one(&input));
    println!("2. {}", part_two(&input));
}

fn parse(input: &str) -> Vec<Instruction> {
    input
        .lines()
        .map(|l| {
            let split = l.split(" = ").collect::<Vec<&str>>();
            let lhs = split[0];
            let value = split[1];
            if lhs == "mask" {
                return Instruction::Mask(
                    value
                        .chars()
                        .map(|c| match c {
                            'X' => None,
                            '0' => Some(false),
                            '1' => Some(true),
                            _ => panic!("Unexpected input"),
                        })
                        .collect(),
                );
            } else {
                return Instruction::Mem {
                    destination: lhs
                        .strip_prefix("mem[")
                        .unwrap()
                        .strip_suffix("]")
                        .unwrap()
                        .parse()
                        .unwrap(),
                    value: value.parse().unwrap(),
                };
            }
        })
        .collect()
}

fn part_one(input: &[Instruction]) -> usize {
    let mut memory: HashMap<usize, usize> = HashMap::new();

    let mut mask = vec![];
    for instruction in input {
        match instruction {
            Instruction::Mask(new_mask) => {
                mask = new_mask.to_owned();
            }
            Instruction::Mem { destination, value } => {
                let mut masked_value = *value;
                for (idx, bit) in mask.iter().enumerate().filter(|(_, m)| m.is_some()) {
                    let bitmask = (1 as usize) << (35 - idx);
                    if bit.unwrap() {
                        masked_value |= bitmask;
                    } else {
                        masked_value &= !bitmask;
                    }
                }
                memory.insert(*destination, masked_value);
            }
        }
    }

    memory.values().sum()
}

fn part_two(input: &[Instruction]) -> usize {
    let mut memory: HashMap<usize, usize> = HashMap::new();

    let mut mask = vec![];
    for instruction in input {
        match instruction {
            Instruction::Mask(new_mask) => {
                mask = new_mask.to_owned();
            }
            Instruction::Mem { destination, value } => {
                let mut masked_destination = *destination;
                for (bit_idx, _) in mask
                    .iter()
                    .enumerate()
                    .filter(|&(_, bit)| *bit == Some(true))
                {
                    masked_destination |= (1 as usize) << (35 - bit_idx);
                }

                let mut masked_destinations = vec![masked_destination];
                for (bit_idx, _) in mask.iter().enumerate().filter(|&(_, bit)| *bit == None) {
                    let bitmask = (1 as usize) << (35 - bit_idx);
                    let len = masked_destinations.len();
                    for addr_idx in 0..len {
                        masked_destinations.push(masked_destinations[addr_idx] & !bitmask);
                        masked_destinations[addr_idx] |= bitmask;
                    }
                }

                for addr in &masked_destinations {
                    memory.insert(*addr, *value);
                }
            }
        }
    }

    memory.values().sum()
}
