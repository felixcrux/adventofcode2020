use std::collections::HashSet;

fn main() {
    let input_str = std::fs::read_to_string("data/13.txt").unwrap();

    let mut lines = input_str.lines();
    let departure = lines.next().unwrap().parse::<usize>().unwrap();
    let buses = lines
        .next()
        .unwrap()
        .split(',')
        .filter(|&b| b != "x")
        .map(|s| s.parse::<usize>().unwrap())
        .collect::<HashSet<usize>>();

    let mut lines = input_str.lines();
    lines.next();
    let buses2 = lines
        .next()
        .unwrap()
        .split(',')
        .map(|x| x.to_owned())
        .collect::<Vec<String>>();

    println!("1. {}", part_one(departure, &buses));
    println!("2. {}", part_two(&buses2));
}

fn part_one(departure: usize, buses: &HashSet<usize>) -> usize {
    let mut min_bus = 0;
    let mut min_wait = usize::MAX;
    for bus in buses {
        if bus - (departure % bus) < min_wait {
            min_wait = bus - (departure % bus);
            min_bus = *bus;
        }
    }

    min_bus * min_wait
}

fn part_two(input: &[String]) -> usize {
    let buses = input
        .iter()
        .enumerate()
        .filter(|(_, b)| *b != "x")
        .map(|(idx, val)| {
            let bus = val.parse::<usize>().unwrap();
            (idx, bus)
        })
        .collect::<Vec<(usize, usize)>>();

    let mut period = buses[0].1;
    let mut cur_time = buses[0].0;
    for (offset, bus) in &buses[1..] {
        while (cur_time + *offset) % bus != 0 {
            cur_time += period;
        }
        period *= bus;
    }

    cur_time
}
