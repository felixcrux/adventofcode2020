use std::fs;
use std::io::Read;

#[derive(Debug, Eq, PartialEq, Ord, PartialOrd)]
struct Seat {
    row: u32,
    col: u32,
    id: u32,
}

fn main() {
    let mut file = fs::File::open("data/5.txt").unwrap();
    let mut input_str = String::new();
    file.read_to_string(&mut input_str).unwrap();

    let seats = input_str.lines().map(|pass| {
        let mut row = 0;
        let mut col = 0;

        for char in pass.chars() {
            match char {
                'B' => row = (row * 2) + 1,
                'F' => row *= 2,
                'R' => col = (col * 2) + 1,
                'L' => col *= 2,
                _ => panic!("Unknown char"),
            }
        }

        Seat {
            row,
            col,
            id: row * 8 + col,
        }
    });

    println!("1. {}", part_one(seats.clone()));
    println!("2. {}", part_two(seats));
}

fn part_one(input: impl Iterator<Item = Seat>) -> u32 {
    input.max_by_key(|s| s.id).unwrap().id
}

fn part_two(input: impl Iterator<Item = Seat>) -> u32 {
    let mut input = input.collect::<Vec<Seat>>();
    input.sort_unstable();

    let mut prev = None;
    for seat in input {
        if let Some(prev) = prev {
            if seat.id > prev + 1 {
                return seat.id - 1;
            }
        }
        prev = Some(seat.id);
    }

    panic!("No solution");
}
