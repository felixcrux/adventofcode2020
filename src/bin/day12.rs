fn main() {
    let input_str = std::fs::read_to_string("data/12.txt").unwrap();
    let input = parse(&input_str);

    println!("1. {}", part_one(&input));
    println!("2. {}", part_two(&input));
}

fn parse(input: &str) -> Vec<(char, isize)> {
    input
        .lines()
        .map(|line| {
            (
                line.chars().nth(0).unwrap(),
                line[1..].parse::<isize>().unwrap(),
            )
        })
        .collect()
}

fn part_one(input: &[(char, isize)]) -> isize {
    let headings = ['N', 'E', 'S', 'W'];

    let mut position = (0, 0);
    let mut current_heading_idx: usize = 1;

    for (action, magnitude) in input {
        match action {
            'R' => {
                current_heading_idx =
                    (current_heading_idx + (*magnitude as usize / 90)) % headings.len()
            }
            'L' => {
                let new_heading_idx = (current_heading_idx as isize - (magnitude / 90) as isize
                    + headings.len() as isize)
                    % headings.len() as isize;
                current_heading_idx = new_heading_idx as usize;
            }
            'F' => travel(headings[current_heading_idx], *magnitude, &mut position),
            _ => travel(*action, *magnitude, &mut position),
        }
    }

    position.0.abs() + position.1.abs()
}

fn travel(direction: char, distance: isize, current_position: &mut (isize, isize)) {
    match direction {
        'N' => current_position.1 += distance,
        'E' => current_position.0 += distance,
        'S' => current_position.1 -= distance,
        'W' => current_position.0 -= distance,
        _ => panic!("Unknown direction"),
    }
}

fn part_two(input: &[(char, isize)]) -> isize {
    let mut ship_position = (0, 0);
    let mut waypoint_position = (10, 1);

    for (action, magnitude) in input {
        match action {
            'R' => {
                for _ in 0..(magnitude / 90) {
                    let new_x = waypoint_position.1;
                    let new_y = -waypoint_position.0;
                    waypoint_position = (new_x, new_y);
                }
            }
            'L' => {
                for _ in 0..(magnitude / 90) {
                    let new_x = -waypoint_position.1;
                    let new_y = waypoint_position.0;
                    waypoint_position = (new_x, new_y);
                }
            }
            'F' => {
                ship_position.0 += waypoint_position.0 * magnitude;
                ship_position.1 += waypoint_position.1 * magnitude;
            }
            _ => travel(*action, *magnitude, &mut waypoint_position),
        }
    }

    ship_position.0.abs() + ship_position.1.abs()
}
