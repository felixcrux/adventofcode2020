use std::collections::{HashMap, HashSet};
use std::fs;
use std::io::Read;

use regex::Regex;

fn main() {
    let mut file = fs::File::open("data/4.txt").unwrap();
    let mut input_str = String::new();
    file.read_to_string(&mut input_str).unwrap();

    let passports = input_str
        .split("\n\n")
        .map(|raw_passport| {
            let mut passport = HashMap::new();
            raw_passport.split_ascii_whitespace().for_each(|field| {
                let mut field_and_value = field.split(':');
                passport.insert(
                    field_and_value.next().unwrap(),
                    field_and_value.next().unwrap(),
                );
            });
            passport
        })
        .collect::<Vec<HashMap<&str, &str>>>();

    println!("1. {}", part_one(&passports));
    println!("2. {}", part_two(&passports));
}

fn part_one(passports: &[HashMap<&str, &str>]) -> usize {
    let required_fields: HashSet<&'static str> = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]
        .iter()
        .cloned()
        .collect();

    passports
        .iter()
        .filter(|passport| {
            passport
                .keys()
                .cloned()
                .collect::<HashSet<&str>>()
                .is_superset(&required_fields)
        })
        .count()
}

fn part_two(passports: &[HashMap<&str, &str>]) -> usize {
    let hair_colour_regex = Regex::new(r"^#[0-9a-f]{6}$").unwrap();
    let eye_colour_regex = Regex::new(r"^(amb|blu|brn|gry|grn|hzl|oth)$").unwrap();
    let passport_id_regex = Regex::new(r"^\d{9}$").unwrap();

    passports
        .iter()
        .filter(|passport| {
            if let Some(byr) = passport.get("byr") {
                if !(byr.len() == 4 && byr >= &"1920" && byr <= &"2002") {
                    return false;
                }
            } else {
                return false;
            }
            if let Some(iyr) = passport.get("iyr") {
                if !(iyr.len() == 4 && *iyr >= "2010" && *iyr <= "2020") {
                    return false;
                }
            } else {
                return false;
            }
            if let Some(eyr) = passport.get("eyr") {
                if !(eyr.len() == 4 && *eyr >= "2020" && *eyr <= "2030") {
                    return false;
                }
            } else {
                return false;
            }
            if let Some(hgt) = passport.get("hgt") {
                if hgt.ends_with("cm") {
                    let hgt = hgt.trim_end_matches("cm");
                    if !(hgt.len() == 3 && hgt >= "150" && hgt <= "193") {
                        return false;
                    }
                } else if hgt.ends_with("in") {
                    let hgt = hgt.trim_end_matches("in");
                    if !(hgt.len() == 2 && hgt >= "59" && hgt <= "76") {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
            if let Some(hcl) = passport.get("hcl") {
                if !hair_colour_regex.is_match(hcl) {
                    return false;
                }
            } else {
                return false;
            }
            if let Some(ecl) = passport.get("ecl") {
                if !eye_colour_regex.is_match(ecl) {
                    return false;
                }
            } else {
                return false;
            }
            if let Some(pid) = passport.get("pid") {
                if !passport_id_regex.is_match(pid) {
                    return false;
                }
            } else {
                return false;
            }
            return true;
        })
        .count()
}
