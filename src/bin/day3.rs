use std::fs;
use std::io::{self, BufRead};

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
enum Terrain {
    Open,
    Tree,
}

fn main() {
    let file = fs::File::open("data/3.txt").unwrap();
    let input = io::BufReader::new(file)
        .lines()
        .map(|line| {
            line.unwrap()
                .chars()
                .map(|c| match c {
                    '.' => Terrain::Open,
                    '#' => Terrain::Tree,
                    _ => panic!("Unknown terrain"),
                })
                .collect::<Vec<Terrain>>()
        })
        .collect::<Vec<Vec<Terrain>>>();

    println!("1. {}", part_one(&input));
    println!("2. {}", part_two(&input));
}

fn part_one(input: &[Vec<Terrain>]) -> usize {
    count_trees_on_slope(input, 0, 0, 1, 3)
}

fn part_two(input: &[Vec<Terrain>]) -> usize {
    count_trees_on_slope(input, 0, 0, 1, 1)
        * count_trees_on_slope(input, 0, 0, 1, 3)
        * count_trees_on_slope(input, 0, 0, 1, 5)
        * count_trees_on_slope(input, 0, 0, 1, 7)
        * count_trees_on_slope(input, 0, 0, 2, 1)
}

fn count_trees_on_slope(
    terrain: &[Vec<Terrain>],
    start_x: usize,
    start_y: usize,
    x_step: usize,
    y_step: usize,
) -> usize {
    let mut tree_count = 0;

    let mut x = start_x;
    let mut y = start_y;
    let max_x = terrain.len();
    let max_y = terrain[0].len();

    while x < max_x {
        if terrain[x][y] == Terrain::Tree {
            tree_count += 1;
        }

        y = (y + y_step) % max_y;
        x += x_step;
    }

    return tree_count;
}
