use std::collections::{HashMap, HashSet};
use std::fs;
use std::io::Read;

use regex::Regex;

type Rule = (String, Vec<(usize, String)>);

fn main() {
    let mut file = fs::File::open("data/7.txt").unwrap();
    let mut input_str = String::new();
    file.read_to_string(&mut input_str).unwrap();

    let rules = parse(&input_str);

    println!("1. {}", part_one(&rules));
    println!("2. {}", part_two(&rules));
}

fn parse(input: &str) -> Vec<Rule> {
    let container_re = Regex::new(r"^(?P<container>.+) bags contain ").unwrap();
    let contained_re = Regex::new(r"((\d+) ([^,]+)) bags?(,|\.)").unwrap();

    input
        .lines()
        .map(|line| {
            let container = container_re
                .captures(&line)
                .unwrap()
                .name("container")
                .unwrap()
                .as_str()
                .to_string();

            let contained = contained_re
                .captures_iter(&line)
                .map(|capture| (capture[2].parse::<usize>().unwrap(), capture[3].to_string()))
                .collect();
            (container, contained)
        })
        .collect()
}

fn part_one(rules: &[Rule]) -> usize {
    let mut containment_map: HashMap<String, HashSet<String>> = HashMap::new();
    for rule in rules {
        let container = &rule.0;
        for (_, contained) in &rule.1 {
            if !containment_map.contains_key(contained) {
                containment_map.insert(contained.to_owned(), HashSet::new());
            }
            containment_map
                .get_mut(contained)
                .unwrap()
                .insert(container.to_owned());
        }
    }

    let mut results = HashSet::<String>::new();
    let mut remaining_bag_types_to_search = vec!["shiny gold".to_string()];

    while let Some(bag_type) = remaining_bag_types_to_search.pop() {
        if let Some(containers) = containment_map.get(&bag_type) {
            for container in containers.iter() {
                if !results.contains(container) {
                    results.insert(container.clone());
                    remaining_bag_types_to_search.push(container.to_owned());
                }
            }
        }
    }

    results.len()
}

fn part_two(rules: &[Rule]) -> usize {
    count_nested_bags("shiny gold", &rules.iter().cloned().collect())
}

fn count_nested_bags(
    parent_bag: &str,
    containment_map: &HashMap<String, Vec<(usize, String)>>,
) -> usize {
    if let Some(nested_bags) = containment_map.get(parent_bag) {
        return nested_bags
            .iter()
            .map(|(count, bag_type)| count + count * count_nested_bags(bag_type, containment_map))
            .sum();
    } else {
        return 0;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_single_rule_no_contained_bags() {
        let input = "faded blue bags contain no other bags.";
        let expected: Vec<Rule> = vec![("faded blue".to_string(), vec![])];
        assert_eq!(parse(input), expected);
    }

    #[test]
    fn test_parse_single_rule_one_contained_bag_of_one_type() {
        let input = "bright white bags contain 1 shiny gold bag.";
        let expected: Vec<Rule> = vec![(
            "bright white".to_string(),
            vec![(1, "shiny gold".to_string())],
        )];
        assert_eq!(parse(input), expected);
    }

    #[test]
    fn test_parse_single_rule_multiple_contained_bags_of_one_type() {
        let input = "dark olive bags contain 3 faded blue bags.";
        let expected: Vec<Rule> = vec![(
            "dark olive".to_string(),
            vec![(3, "faded blue".to_string())],
        )];
        assert_eq!(parse(input), expected);
    }

    #[test]
    fn test_parse_single_rule_multiple_contained_bags() {
        let input = "muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.";
        let expected: Vec<Rule> = vec![(
            "muted yellow".to_string(),
            vec![(2, "shiny gold".to_string()), (9, "faded blue".to_string())],
        )];
        assert_eq!(parse(input), expected);
    }

    #[test]
    fn test_parse_multiple_rules() {
        let input = "light red bags contain 1 bright white bag, 2 muted yellow bags.\n\
                     dark orange bags contain 3 bright white bags, 4 muted yellow bags.\n\
                     bright white bags contain 1 shiny gold bag.\n\
                     faded blue bags contain no other bags.";
        let expected: Vec<Rule> = vec![
            (
                "light red".to_string(),
                vec![
                    (1, "bright white".to_string()),
                    (2, "muted yellow".to_string()),
                ],
            ),
            (
                "dark orange".to_string(),
                vec![
                    (3, "bright white".to_string()),
                    (4, "muted yellow".to_string()),
                ],
            ),
            (
                "bright white".to_string(),
                vec![(1, "shiny gold".to_string())],
            ),
            ("faded blue".to_string(), vec![]),
        ];
        assert_eq!(parse(input), expected);
    }
}
