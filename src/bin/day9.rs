use std::collections::HashSet;
use std::fs;
use std::io::Read;

fn main() {
    let mut file = fs::File::open("data/9.txt").unwrap();
    let mut input_str = String::new();
    file.read_to_string(&mut input_str).unwrap();

    let input = input_str
        .lines()
        .map(|line| line.parse::<usize>().unwrap())
        .collect::<Vec<usize>>();

    let part_one_solution = part_one(&input);

    println!("1. {}", part_one_solution);
    println!("2. {}", part_two(&input, part_one_solution));
}

fn part_one(input: &[usize]) -> usize {
    let window_size = 25;

    let mut head = input.iter();
    let mut tail = input.iter();

    let mut window = (0..window_size)
        .map(|_| head.next().unwrap())
        .copied()
        .collect::<HashSet<usize>>();

    for &next in head {
        let mut found = false;
        for &candidate in &window {
            if next > candidate && window.contains(&(next - candidate)) {
                found = true;
                break;
            }
        }
        if !found {
            return next;
        }

        window.remove(tail.next().unwrap());
        window.insert(next);
    }

    panic!("No solution")
}

fn part_two(input: &[usize], target: usize) -> usize {
    let mut tail_idx = 0;
    let mut head_idx = 1;

    loop {
        let sum = (tail_idx..=head_idx).map(|idx| input[idx]).sum();

        if target == sum {
            break;
        }
        if target > sum {
            head_idx += 1;
        }
        if target < sum {
            tail_idx += 1;
        }
    }

    let mut max = 0;
    let mut min = usize::MAX;
    (tail_idx..=head_idx).map(|idx| input[idx]).for_each(|val| {
        if val < min {
            min = val;
        }
        if val > max {
            max = val;
        }
    });

    min + max
}
