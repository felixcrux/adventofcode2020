use std::collections;
use std::fs;
use std::io::{self, BufRead};

fn main() {
    let file = fs::File::open("data/1.txt").unwrap();
    let input = io::BufReader::new(file)
        .lines()
        .collect::<Result<Vec<String>, _>>()
        .unwrap();

    println!("1. {}", part_one(input.iter()));
    println!("2. {}", part_two(input.iter()));
}

fn part_one<'a>(input: impl Iterator<Item = &'a String>) -> i32 {
    let mut seen = collections::HashSet::<i32>::new();
    for expense_amount_as_str in input {
        let expense_amount = expense_amount_as_str.parse::<i32>().unwrap();
        let counterpart = 2020 - expense_amount;
        if seen.contains(&counterpart) {
            return expense_amount * counterpart;
        }
        seen.insert(expense_amount);
    }
    panic!("No solution");
}

fn part_two<'a>(input: impl Iterator<Item = &'a String>) -> i32 {
    let expense_amounts = input
        .map(|s| s.parse::<i32>().unwrap())
        .collect::<collections::HashSet<i32>>();
    for expense_amount in &expense_amounts {
        let sub_target = 2020 - expense_amount;
        let sub_result = part_two_helper(sub_target, &expense_amounts);
        if let Some(sub_result) = sub_result {
            return expense_amount * sub_result.0 * sub_result.1;
        }
    }
    panic!("No solution");
}

fn part_two_helper(target: i32, expense_amounts: &collections::HashSet<i32>) -> Option<(i32, i32)> {
    for expense_amount in expense_amounts {
        let counterpart = target - expense_amount;
        if expense_amounts.contains(&counterpart) {
            return Some((*expense_amount, counterpart));
        }
    }
    return None;
}
