fn main() {
    let input_str = include_str!("../../data/17.txt");

    let input = parse(&input_str);

    println!("1. {}", part_one(&input));
    println!("2. {}", part_two(&input));
}

fn parse(input: &str) -> Vec<Vec<u8>> {
    input
        .lines()
        .map(|l| {
            l.chars()
                .map(|c| match c {
                    '.' => 0 as u8,
                    '#' => 1 as u8,
                    _ => panic!("Unexpected input"),
                })
                .collect()
        })
        .collect()
}

fn tick_3d(world: &[[[u8; 22]; 22]; 22]) -> [[[u8; 22]; 22]; 22] {
    let mut new_world = world.clone();

    for x in 1..21 {
        for y in 1..21 {
            for z in 1..21 {
                let current_state = world[z][x][y];
                let active_near = ((z - 1)..=(z + 1))
                    .map(|z_p| {
                        ((x - 1)..=(x + 1))
                            .map(|x_p| {
                                ((y - 1)..=(y + 1))
                                    .map(|y_p| world[z_p][x_p][y_p])
                                    .sum::<u8>()
                            })
                            .sum::<u8>()
                    })
                    .sum::<u8>()
                    - current_state;
                if current_state == 1 {
                    if !(active_near == 3 || active_near == 2) {
                        new_world[z][x][y] = 0;
                    }
                } else if current_state == 0 {
                    if active_near == 3 {
                        new_world[z][x][y] = 1;
                    }
                }
            }
        }
    }

    new_world
}

fn part_one(input: &[Vec<u8>]) -> usize {
    let mut world = [[[0 as u8; 22]; 22]; 22];

    input.iter().enumerate().for_each(|(x, y_input)| {
        y_input
            .iter()
            .enumerate()
            .for_each(|(y, state)| world[11][7 + x][7 + y] = *state)
    });

    for _ in 0..6 {
        world = tick_3d(&world);
    }

    let mut active = 0;
    for x in 1..21 {
        for y in 1..21 {
            for z in 1..21 {
                active += world[z][x][y] as usize;
            }
        }
    }
    active
}

fn tick_4d(world: &[[[[u8; 22]; 22]; 22]; 22]) -> [[[[u8; 22]; 22]; 22]; 22] {
    let mut new_world = world.clone();

    for w in 1..21 {
        for x in 1..21 {
            for y in 1..21 {
                for z in 1..21 {
                    let current_state = world[w][z][x][y];
                    let active_near = ((w - 1)..=(w + 1))
                        .map(|w_p| {
                            ((z - 1)..=(z + 1))
                                .map(|z_p| {
                                    ((x - 1)..=(x + 1))
                                        .map(|x_p| {
                                            ((y - 1)..=(y + 1))
                                                .map(|y_p| world[w_p][z_p][x_p][y_p])
                                                .sum::<u8>()
                                        })
                                        .sum::<u8>()
                                })
                                .sum::<u8>()
                        })
                        .sum::<u8>()
                        - current_state;
                    if current_state == 1 {
                        if !(active_near == 3 || active_near == 2) {
                            new_world[w][z][x][y] = 0;
                        }
                    } else if current_state == 0 {
                        if active_near == 3 {
                            new_world[w][z][x][y] = 1;
                        }
                    }
                }
            }
        }
    }

    new_world
}

fn part_two(input: &[Vec<u8>]) -> usize {
    let mut world = [[[[0 as u8; 22]; 22]; 22]; 22];

    input.iter().enumerate().for_each(|(x, y_input)| {
        y_input
            .iter()
            .enumerate()
            .for_each(|(y, state)| world[11][11][7 + x][7 + y] = *state)
    });

    for _ in 0..6 {
        world = tick_4d(&world);
    }

    let mut active = 0;
    for w in 1..21 {
        for x in 1..21 {
            for y in 1..21 {
                for z in 1..21 {
                    active += world[w][z][x][y] as usize;
                }
            }
        }
    }
    active
}
