use std::collections::HashSet;
use std::fs;
use std::io::Read;

fn main() {
    let mut file = fs::File::open("data/6.txt").unwrap();
    let mut input_str = String::new();
    file.read_to_string(&mut input_str).unwrap();

    let parsed = parse(&input_str);

    println!("1. {}", part_one(&parsed));
    println!("2. {}", part_two(&parsed));
}

fn parse(input: &str) -> Vec<Vec<HashSet<char>>> {
    input
        .split("\n\n")
        .map(|group| {
            group
                .lines()
                .map(|individual| {
                    let mut answered_questions = HashSet::new();
                    individual.chars().for_each(|c| {
                        answered_questions.insert(c);
                    });
                    answered_questions
                })
                .collect()
        })
        .collect()
}

fn count_distinct_answers_in_group(group_answers: &[HashSet<char>]) -> usize {
    let mut iter = group_answers.iter();
    let first = iter.next().unwrap().clone();
    iter.fold(first, |acc, next| acc.union(&next).copied().collect())
        .len()
}

fn count_shared_answers_in_group(group_answers: &[HashSet<char>]) -> usize {
    let mut iter = group_answers.iter();
    let first = iter.next().unwrap().clone();
    iter.fold(first, |acc, next| {
        acc.intersection(&next).copied().collect()
    })
    .len()
}

fn part_one(input: &[Vec<HashSet<char>>]) -> usize {
    input
        .iter()
        .map(|group| count_distinct_answers_in_group(group))
        .sum()
}

fn part_two(input: &[Vec<HashSet<char>>]) -> usize {
    input
        .iter()
        .map(|group| count_shared_answers_in_group(group))
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_single_group_single_individual() {
        let input = "abc";
        let expected: Vec<Vec<HashSet<char>>> =
            vec![vec![['a', 'b', 'c'].iter().cloned().collect()]];
        assert_eq!(parse(input), expected);
    }

    #[test]
    fn test_parse_single_group_multiple_individuals() {
        let input = "abc\n\
                     cd\n\
                     d";
        let expected: Vec<Vec<HashSet<char>>> = vec![vec![
            ['a', 'b', 'c'].iter().cloned().collect(),
            ['c', 'd'].iter().cloned().collect(),
            ['d'].iter().cloned().collect(),
        ]];
        assert_eq!(parse(input), expected);
    }

    #[test]
    fn test_parse_multiple_groups_multiple_individuals() {
        let input = "abc\n\
                     cd\n\
                     \n\
                     a\n\
                     a\n\
                     a";
        let expected: Vec<Vec<HashSet<char>>> = vec![
            vec![
                ['a', 'b', 'c'].iter().cloned().collect(),
                ['c', 'd'].iter().cloned().collect(),
            ],
            vec![
                ['a'].iter().cloned().collect(),
                ['a'].iter().cloned().collect(),
                ['a'].iter().cloned().collect(),
            ],
        ];
        assert_eq!(parse(input), expected);
    }

    #[test]
    fn test_count_distinct_answers_in_group() {
        let input: Vec<HashSet<char>> = vec![
            ['a', 'b', 'c'].iter().cloned().collect(),
            ['c', 'd'].iter().cloned().collect(),
        ];
        assert_eq!(count_distinct_answers_in_group(&input), 4);
    }

    #[test]
    fn test_count_shared_answers_in_group() {
        let input: Vec<HashSet<char>> = vec![
            ['a', 'b', 'c'].iter().cloned().collect(),
            ['c', 'd'].iter().cloned().collect(),
        ];
        assert_eq!(count_shared_answers_in_group(&input), 1);
    }
}
