use std::collections::HashMap;
use std::fs;
use std::io::Read;

fn main() {
    let mut file = fs::File::open("data/10.txt").unwrap();
    let mut input_str = String::new();
    file.read_to_string(&mut input_str).unwrap();

    let input = parse(&input_str);

    println!("1. {}", part_one(&input));

    let mut seen: HashMap<usize, usize> = HashMap::new();
    println!("2. {}", part_two(&input, &mut seen, 0));
}

fn parse(input: &str) -> Vec<usize> {
    let mut parsed = input
        .lines()
        .map(|line| line.parse::<usize>().unwrap())
        .collect::<Vec<usize>>();
    parsed.sort_unstable();
    parsed.push(parsed[parsed.len() - 1] + 3);
    parsed
}

fn part_one(input: &[usize]) -> usize {
    let mut one_diffs = 0;
    let mut three_diffs = 0;
    let mut prev = 0;
    for &adapter in input {
        let diff = adapter - prev;
        if diff == 1 {
            one_diffs += 1;
        } else if diff == 3 {
            three_diffs += 1;
        }
        prev = adapter;
    }

    one_diffs * three_diffs
}

fn part_two(remaining: &[usize], seen: &mut HashMap<usize, usize>, current: usize) -> usize {
    if remaining.len() == 1 {
        return 1;
    }

    let mut possible_permutations = 0;

    for (idx_offset, possible_next) in remaining.iter().enumerate() {
        if possible_next - current > 3 {
            break;
        }
        if !seen.contains_key(&possible_next) {
            let subsequent_possible_permutations =
                part_two(&remaining[(idx_offset + 1)..], seen, remaining[idx_offset]);
            seen.insert(*possible_next, subsequent_possible_permutations);
        }
        possible_permutations += seen.get(&possible_next).unwrap();
    }

    return possible_permutations;
}
